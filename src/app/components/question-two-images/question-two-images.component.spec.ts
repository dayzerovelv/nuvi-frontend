import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionTwoImagesComponent } from './question-two-images.component';

describe('QuestionTwoImagesComponent', () => {
  let component: QuestionTwoImagesComponent;
  let fixture: ComponentFixture<QuestionTwoImagesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuestionTwoImagesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionTwoImagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

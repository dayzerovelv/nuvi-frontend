import { Component, OnInit } from '@angular/core';
import { slideInOutAnimation } from '../../animations/slide-in-out.animation';
import { trigger, state, style, transition, animate, keyframes } from '@angular/animations';
import { Router } from '@angular/router';
import { Question } from '../../models/question.model';
import { StepperService } from '../../services/stepper.service';
import { QuestionsService } from '../../services/questions.service';

@Component({
  selector: 'app-question-two-images.component',
  templateUrl: './question-two-images.component.html',
  styleUrls: ['./question-two-images.component.scss'],
  animations: [slideInOutAnimation,
    trigger('dropIntro', [
      state('void', style({ opacity: 1, 'z-index': 100})),
      state('ready', style({ opacity: 0, 'z-index': 0})),
      transition('void => ready', [
        style({
          opacity: 1
        }),
        animate('1000ms 100ms ease-in-out')
      ]),
      transition('ready => void', [
        style({
          opacity: 0
        }),
        animate('1000ms 100ms ease-in-out')
      ]),
    ]),
    trigger('arrowAcontainer', [
      state('void', style({ opacity: 0, 'z-index': 100})),
      state('ready', style({ opacity: 1, 'z-index': 0})),
      transition('void => ready', [
        style({
          opacity: 1
        }),
        animate('1000ms 1000ms ease-in-out')
      ]),
      transition('ready => void', [
        style({
          opacity: 0
        }),
        animate('1000ms 1000ms ease-in-out')
      ]),
    ]),
    trigger('arrowBcontainer', [
      state('void', style({ opacity: 0})),
      state('ready', style({ opacity: 1})),
      transition('void => ready', [
        style({
          opacity: 1
        }),
        animate('1000ms 1000ms ease-in-out')
      ]),
      transition('ready => void', [
        style({
          opacity: 0
        }),
        animate('1000ms 1000ms ease-in-out')
      ]),
    ]),
    trigger('question', [
      state('void', style({ opacity: 0})),
      state('ready', style({ opacity: 1})),
      transition('void => ready', [
        style({
          opacity: 0
        }),
        animate('500ms 800ms ease-in-out')
      ]),
      transition('ready => void', [
        style({
          opacity: 1
        }),
        animate('500ms 0s ease-in-out')
      ])
    ]),
    trigger('leftQuote', [
      state('void', style({ opacity: 0})),
      state('ready', style({ opacity: 1})),
      transition('void => ready', [
        style({
          opacity: 0
        }),
        animate('500ms 1300ms ease-in-out')
      ]),
      transition('ready => void', [
        style({
          opacity: 1
        }),
        animate('500ms 0s ease-in-out')
      ])
    ]),
    trigger('rightQuote', [
      state('void', style({ opacity: 0})),
      state('ready', style({ opacity: 1})),
      transition('void => ready', [
        style({
          opacity: 0
        }),
        animate('500ms 1300ms ease-in-out')
      ]),
      transition('ready => void', [
        style({
          opacity: 1
        }),
        animate('500ms 0s ease-in-out')
      ])
    ]),
  ],
  host: {'[@slideInOutAnimation]': ''}
})
export class QuestionTwoImagesComponent implements OnInit {

  backgroundAnimationConfig: object
  public arrowAnimationConfigA: Object
  public arrowAnimationConfigB: Object
  public arrowAnimationConfigALoop: Object
  public arrowAnimationConfigBLoop: Object
  private arrowA: any;
  private arrowB: any;
  private arrowALoop: any;
  private arrowBLoop: any;
  dropIntro: string = 'void'
  arrowAcontainer: string = 'void'
  arrowBcontainer: string = 'void'
  leftImageUrl: string
  rightImageUrl: string
  questionState: string = 'void'
  leftBlendColor: string = 'rgba(109, 109, 109, 0.753)'
  rightBlendColor: string = 'rgba(109, 109, 109, 0.753)'
  leftQuoteState: string = 'void'
  rightQuoteState: string = 'void'
  selectOptionState: string = 'void'
  questionObject: Question
  hoverSizeLeft: string
  hoverSizeRight: string
  anim: any
  userQuestions: any
  imageList: any
  image1: string
  image2: string

  constructor(
    private router: Router,
    private stepperService: StepperService,
    private questionsService: QuestionsService,
  ) {
    this.getCurrentQuestion()

      this.arrowAnimationConfigA = {
          path: 'assets/anim-json/00_buttonA.json',
          autoplay: false,
          loop: false
      };
      this.arrowAnimationConfigALoop = {
          path: 'assets/anim-json/00_buttonA_loop.json',
          autoplay: true,
          loop: true
      };
      this.arrowAnimationConfigB = {
          path: 'assets/anim-json/00_buttonB.json',
          autoplay: true,
          loop: true
      };
      this.arrowAnimationConfigBLoop = {
          path: 'assets/anim-json/00_buttonB_loop.json',
          autoplay: true,
          loop: true
      };
  }

  ngOnInit() {
    this.userQuestions = JSON.parse(sessionStorage.getItem('questionsList'))

    this.stepperService.getImages().then((res)=>{
      this.imageList = res;
      let thisImage = this.imageList.filter(item => item.step === this.questionObject.step)
      this.image1 = thisImage[0].image1.src;
      this.image2 = thisImage[0].image2.src;
    })

    setTimeout(() =>{
      let option1 = <HTMLElement>document.querySelector('#option1');
      option1.style['pointer-events'] = 'auto';
      let option2 = <HTMLElement>document.querySelector('#option2');
      option2.style['pointer-events'] = 'auto';
      this.dropIntro = 'ready'
      this.arrowAcontainer = 'ready'
      this.arrowBcontainer = 'ready'
      this.questionState = 'ready'
      this.leftQuoteState = 'ready'
      this.rightQuoteState = 'ready'
    }, 500)


    setTimeout(() =>{
      this.selectOptionState = 'ready'
    }, 1500)

    this.setInitialImagens()
    this.getCurrentQuestion()
  }

  handleArrowA(anim: any) {
    this.arrowA = anim;
  }

  handleArrowALoop(anim: any) {
    this.arrowALoop = anim;
  }


  handleArrowB(anim: any) {
    this.arrowB = anim;
  }

  handleArrowBLoop(anim: any) {
    this.arrowBLoop = anim;
  }


  getCurrentQuestion(){
    this.questionObject = this.stepperService.getCurrentQuestion()
  }

  goToNextStep() {
    let nextQuestion = this.stepperService.getNextQuestion()
    if((nextQuestion.step - 1) % 4 === 0){
      this.router.navigate(['0OB1cztzwtmxwVQVlD00FPdocl1jeV8bZKUxtDbzb09J7FAgLq'])
    }else {
      if(this.stepperService.detectmob()){
        this.router.navigate([nextQuestion.templateMobile])
      }else{
        if(this.stepperService.detectIe()){
          this.router.navigate([nextQuestion.templateIE])
        }else{
          this.router.navigate([nextQuestion.template])
        }
      }
    }
  }

  setInitialImagens() {
    this.leftBlendColor = '#e5213b96'
    this.rightBlendColor = '#e5213b96'
    this.hoverSizeRight = "0.5"
    this.hoverSizeLeft = "0.5"
  }

  hoverLeft(){
    this.leftBlendColor = 'rgba(138, 137, 137, 0.452)'
    this.hoverSizeRight = "0.5"
    this.hoverSizeLeft = "0.2"

  }

  hoverRight(){
    this.rightBlendColor = 'rgba(138, 137, 137, 0.452)'
    this.hoverSizeRight = "0.2"
    this.hoverSizeLeft = "0.5"
  }

  selectOptionA(selected) {
    this.rightQuoteState = 'void'
    this.arrowBcontainer = 'void';
    let option1 = <HTMLElement>document.querySelector('#option1');
    option1.style['pointer-events'] = 'none';
    let option2 = <HTMLElement>document.querySelector('#option2');
    option2.style['pointer-events'] = 'none';
      setTimeout(() =>{
      this.dropIntro = 'void'
      this.questionState= 'void'
      this.leftQuoteState ='void'
      this.selectOptionState = 'void'
      this.questionsService.setQuestionsAnswered(this.questionObject.step, selected)
    }, 500)

      setTimeout(() =>{
        this.goToNextStep()
      }, 1800)
  }

  selectOptionB(selected) {
    this.leftQuoteState ='void'
    this.arrowAcontainer = 'void';
    let option1 = <HTMLElement>document.querySelector('#option1');
    option1.style['pointer-events'] = 'none';
    let option2 = <HTMLElement>document.querySelector('#option2');
    option2.style['pointer-events'] = 'none';
      setTimeout(() =>{
      this.dropIntro = 'void'
      this.questionState= 'void'
      this.leftQuoteState ='void'
      this.selectOptionState = 'void'
      this.questionsService.setQuestionsAnswered(this.questionObject.step, selected)
    }, 500)


      setTimeout(() =>{
        this.goToNextStep()
      }, 1800)
  }

}

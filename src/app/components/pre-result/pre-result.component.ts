import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { QuestionsService } from '../../services/questions.service';
import { StepperService } from '../../services/stepper.service';
import { slideInOutAnimation } from '../../animations/slide-in-out.animation';
import { trigger, state, style, transition, animate, keyframes } from '@angular/animations'
import { GoogleCharts } from 'google-charts';

@Component({
  selector: 'app-pre-result',
  templateUrl: './pre-result.component.html',
  styleUrls: ['./pre-result.component.scss'],
  animations: [slideInOutAnimation,
  ],
  host: {'[@slideInOutAnimation]': ''}
})
export class PreResultComponent implements OnInit {

  backVisionarioAnimationConfig: object
  backHeroiAnimationConfig: object
  backGuardiaoAnimationConfig: object
  backEloAnimationConfig: object
  backMentorAnimationConfig: object
  animationConfig: object
  dropIntro: string = 'void'
  timeOut: any
  currentQuestion: any
  split: any
  profileResult: any
  done: any

  constructor(
    private router: Router,
    private stepperService: StepperService,
    private questionsService: QuestionsService) {
      this.animationConfig = {
          path: 'assets/anim-json/nuviLogo_white.json',
          autoplay: true,
          loop: false
      };
      this.backVisionarioAnimationConfig = {
          path: "./assets/anim-json/arch-anims/visionario_lines.json",
          autoplay: true,
          loop: true
      };
      this.backHeroiAnimationConfig = {
          path: "./assets/anim-json/arch-anims/heroi_lines.json",
          autoplay: true,
          loop: true
      };
      this.backGuardiaoAnimationConfig = {
          path: "./assets/anim-json/arch-anims/guardiao_lines.json",
          autoplay: true,
          loop: true
      };
      this.backEloAnimationConfig = {
          path: "./assets/anim-json/arch-anims/elo_lines.json",
          autoplay: true,
          loop: true
      };
      this.backMentorAnimationConfig = {
          path: "./assets/anim-json/arch-anims/mentor_lines.json",
          autoplay: true,
          loop: true
      };
  }

  ngOnInit() {
    this.profileResult = this.questionsService.getProfileResult();



    this.setResults().then(() => {
      setTimeout(()=>{
        this.router.navigate(['results'])
      }, 2000);
    });
  }
 
  float2int (value) {
    return value | 0;
  }

  setResults(){

      let backAnims = [
          {
            'name': 'Explorador',
            'value': this.backVisionarioAnimationConfig
          },
          {
            'name': 'Herói',
            'value': this.backHeroiAnimationConfig
          },
          {
            'name': 'Guardião',
            'value': this.backGuardiaoAnimationConfig
          },
          {
            'name': 'Elo',
            'value': this.backEloAnimationConfig
          },
          {
            'name': 'Mentor',
            'value': this.backMentorAnimationConfig
          }
    ];
    return new Promise((resolve)=>{
      this.questionsService.getArchetypes().subscribe(archetypesList => {
          let archetype
          let data = [];
          let dataColor = [];
          let total = 0;
          
          for(let _i = 0; _i < this.profileResult.length; _i++){
            total += this.profileResult[_i].value;
          }
          
          data.push(['Archetype', 'Amount', '']);
          
          for(let _i = 0; _i < this.profileResult.length; _i++){
             archetype =  archetypesList.filter(item => item.name === this.profileResult[_i].name)
             dataColor.push(archetype[0].font)
             console.log(this.profileResult[_i].value)
             console.log(total)
             console.log((100 * this.profileResult[_i].value) / total)
             data.push([this.profileResult[_i].name,
                        this.profileResult[_i].value,
                       (this.float2int((100 * this.profileResult[_i].value) / total))];
//             100 - total
//              x  - value
//              x= 100 * value / length
          }

          archetype =  archetypesList.filter(item => item.name === this.profileResult[0].name)
          // use this line to simulate the result
          // let archetype =  archetypesList.filter(item => item.name === 'Herói')
          let graphFont = archetype[0].font;
          let thisAnim = backAnims.filter(item => item.name === archetype[0].name)[0].value;
          sessionStorage.setItem('final-result', JSON.stringify(archetype))
          sessionStorage.setItem('backAnimation', JSON.stringify(thisAnim))
          sessionStorage.setItem('graph-data', JSON.stringify(data))
          sessionStorage.setItem('graph-color', JSON.stringify(dataColor))
          sessionStorage.setItem('graph-font', JSON.stringify(graphFont))
          resolve();
        });
    })
  }


  handleAnimation(anim: any) {
    this.split = anim;
    // this.split.addEventListener('loopComplete', () => {
    //   if(this.done){
    //     this.router.navigate(['results'])
    //   }
    // });
  }

}

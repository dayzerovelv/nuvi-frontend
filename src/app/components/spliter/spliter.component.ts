import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { QuestionsService } from '../../services/questions.service';
import { StepperService } from '../../services/stepper.service';
import { slideInOutAnimation } from '../../animations/slide-in-out.animation';
import { trigger, state, style, transition, animate, keyframes } from '@angular/animations'

@Component({
  selector: 'app-spliter',
  templateUrl: './spliter.component.html',
  styleUrls: ['./spliter.component.scss'],
  animations: [slideInOutAnimation,
  ],
  host: {'[@slideInOutAnimation]': ''}
})
export class SpliterComponent implements OnInit {

  animationConfig: object
  dropIntro: string = 'void'
  timeOut: any
  spliters = ['s02.json', 's03.json', 's04.json']
  position = Math.floor(Math.random() * 3);
  currentQuestion: any
  split: any

  constructor(
    private router: Router,
    private stepperService: StepperService,
    private questionsService: QuestionsService) {
      this.animationConfig = {
          path: 'assets/separadores/' + this.spliters[this.position],
          autoplay: false,
          loop: true
      };
  }

  ngOnInit() {
    this.currentQuestion = this.stepperService.getCurrentQuestion()
    setTimeout(()=>{
      this.split.play();
    }, 500)
  }

  handleAnimation(anim: any) {
    this.split = anim;
    this.split.addEventListener('loopComplete', () => {
      this.split.stop();
        if(this.stepperService.detectmob()){
          this.router.navigate([this.currentQuestion.templateMobile])
        }else{
          if(this.stepperService.detectIe()){
            this.router.navigate([this.currentQuestion.templateIE])
          }else{
            this.router.navigate([this.currentQuestion.template])
          }
        }
    });
  }

}

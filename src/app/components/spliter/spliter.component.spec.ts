import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpliterComponent } from './spliter.component';

describe('SpliterComponent', () => {
  let component: SpliterComponent;
  let fixture: ComponentFixture<SpliterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpliterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpliterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

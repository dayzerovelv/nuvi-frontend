import { Component, OnInit, Injector } from '@angular/core';
import { QuestionsService } from './../../services/questions.service';
import { slideInOutAnimation } from '../../animations/slide-in-out.animation';
import { trigger, state, style, transition, animate, keyframes } from '@angular/animations';
import { Howl, Howler } from 'howler';
import { GoogleCharts } from 'google-charts';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.scss'],
  animations: [slideInOutAnimation,
    trigger('dropIntro', [
      state('void', style({ opacity: 1})),
      state('ready', style({ opacity: 1})),
      transition('void => ready', [
        style({
          opacity: 1
        }),
        animate('1000ms 0ms ease-in-out')
      ]),
      transition('ready => void', [
        style({
          opacity: 1
        }),
        animate('1000ms 0ms ease-in-out')
      ]),
    ]),
    trigger('iconNuvi', [
      state('void', style({ opacity: 0,
      transform: 'translate(0, -80%) rotate(180deg)'})),
      state('ready', style({ opacity: 1,
      transform: 'translate(0, -80%) rotate(180deg)'})),
      transition('void => ready', [
        style({
          opacity: 0,
          transform: 'rotate(180deg)'
        }),
        animate('500ms 800ms ease-in-out')
      ]),
      transition('ready => void', [
        style({
          opacity: 1
        }),
        animate('500ms 100ms ease-in-out')
      ]),
    ]),
    trigger('logoNuvi', [
      state('void', style({ opacity: 0})),
      state('ready', style({ opacity: 1,
      transform: 'translate(0, -10%)'})),
      transition('void => ready', [
        style({
          opacity: 0,
          transform: 'translate(0, 40%)'
        }),
        animate('500ms 800ms ease-in-out')
      ]),
    ]),
    trigger('bgImage', [
      state('void', style({})),
      state('ready', style({transform: 'translate(-60%, 25%)'})),
      transition('void => ready', [
        style({
        }),
        animate('500ms 700ms ease-in-out')
      ]),
    ]),
    trigger('profileTitle', [
      state('void', style({ opacity: 0})),
      state('ready', style({ opacity: 1})),
      transition('void => ready', [
        style({
          opacity: 0,
          transform: 'translate(0, 100px)'
        }),
        animate('500ms 800ms ease-in-out')
      ]),
      transition('ready => void', [
        style({
          opacity: 1
        }),
        animate('500ms 100ms ease-in-out')
      ]),
    ]),
    trigger('profileArchetype', [
      state('void', style({ opacity: 0})),
      state('ready', style({ opacity: 1})),
      transition('void => ready', [
        style({
          opacity: 0,
          transform: 'translate(0, 100px)'
        }),
        animate('500ms 900ms ease-in-out')
      ]),
      transition('ready => void', [
        style({
          opacity: 1
        }),
        animate('500ms 100ms ease-in-out')
      ]),
    ]),
    trigger('profileMessage', [
      state('void', style({ opacity: 0})),
      state('ready', style({ opacity: 1})),
      transition('void => ready', [
        style({
          opacity: 0,
          transform: 'translate(0, 100px)'
        }),
        animate('500ms 1000ms ease-in-out')
      ]),
      transition('ready => void', [
        style({
          opacity: 1
        }),
        animate('500ms 100ms ease-in-out')
      ]),
    ]),
    trigger('imageState', [
      state('void', style({ opacity: 0})),
      state('ready', style({ opacity: 1})),
      transition('void => ready', [
        style({
          opacity: 0,
          transform: 'translate(0, 300px)'
        }),
        animate('600ms 500ms ease-in-out')
      ]),
      transition('ready => void', [
        style({
          opacity: 1
        }),
        animate('500ms 100ms ease-in-out')
      ]),
    ]),
    trigger('bgAnim', [
      state('void', style({ opacity: 0})),
      state('ready', style({ opacity: 1})),
      transition('void => ready', [
        style({
          opacity: 0,
          transform: 'translate(0, 300px)'
        }),
        animate('600ms 500ms ease-in-out')
      ]),
      transition('ready => void', [
        style({
          opacity: 1
        }),
        animate('500ms 100ms ease-in-out')
      ]),
    ]),
    trigger('graphics', [
      state('void', style({ opacity: 0})),
      state('ready', style({ opacity: 1})),
      transition('void => ready', [
        style({
          opacity: 0,
          transform: 'translate(0, 300px)',
          'z-index': 40
        }),
        animate('600ms 500ms ease-in-out')
      ]),
    ]),
    trigger('progressMessage', [
      state('void', style({ opacity: 0})),
      state('ready', style({ opacity: 1})),
      transition('void => ready', [
        style({
          opacity: 0,
        }),
        animate('500ms 0ms cubic-bezier(0, 0, 0.20, 1)')
      ]),
      transition('ready => void', [
        style({
          opacity: 1,
        }),
        animate('500ms 0ms cubic-bezier(0, 0, 0.20, 1)')
      ]),
    ]),
    trigger('googleGraphics', [
      state('void', style({ opacity: 0})),
      state('ready', style({ opacity: 1})),
      transition('void => ready', [
        style({
          opacity: 0,
          transform: 'translate(0, 300px)',
          'z-index': 40
        }),
        animate('600ms 500ms ease-in-out')
      ]),
    ]),
  ],
  host: {'[@slideInOutAnimation]': ''}
})
export class ResultsComponent implements OnInit {

  iconAnimationConfig: object
  logoAnimationConfig: object
  backAnimationConfig: object
  private icon: any;
  private logo: any;
  private backAnimation: any;
  dropIntro: string = 'void'
  iconNuvi: string = 'void'
  logoNuvi: string = 'void'
  progressMessage: string = 'void'
  profileTitleState: string = 'void'
  profileArchetypeState: string = 'void'
  profileMessageState: string = 'void'
  wordsState: string = 'void'
  imageState: string = 'void'
  googleGraphics: string = 'void'
  bgImage: string= 'void'
  graphics: string = 'void'
  bgAnim: string = 'void'
  backColor: string
  fontColor: string
  backImage: string
  backAnimPath: string = ''
  gender: string
  result: any
  description: string
  image: string
  userQuestions:any
  questionsJson:any
  ending = {
    'phrase1': '',
    'phrase2': '',
    'phrase3': '',
    'phrase4': ''}
  resultProfile: any
  result_graphic: any
  profileResult: any
  data: any = [];
  dataColor: any = []
  triggerAnim: any
  wordsAnim: any
  voice: Howl

  constructor(
    private questionsService: QuestionsService) {
    this.iconAnimationConfig = {
        path: 'assets/anim-json/resultsLoop.json',
        autoplay: true,
        loop: true
    };
    this.logoAnimationConfig = {
        path: 'assets/anim-json/nuviLogo_white.json',
        autoplay: false,
        loop: false
    };
  }

  ngOnInit() {
    this.setFinalResults();
    this.updateVolume(0.2);
    this.voice = new Howl({
      src: ['./assets/audio/nuvi_voice.mp3'],
      loop: false,
      volume: 1
    });

    window.addEventListener("resize", this.drawChart.bind(this, this.data, this.dataColor, this.fontColor));
    this.voice.play()

    setTimeout(()=>{
      this.dropIntro = 'ready'
      this.iconNuvi = 'ready'
      this.profileTitleState = 'ready'
      this.profileArchetypeState = 'ready'
      this.profileMessageState = 'ready'
      this.imageState = 'ready'
      this.bgAnim = 'ready'
    }, 200)

  }

  handleIcon(anim: any) {
    this.icon = anim;
  }

  handleLogo(anim: any) {
    this.logo = anim;
  }

  handleBack(anim: any) {
    this.backAnimation = anim;
  }

  updateVolume(value) {
    Howler._howls[0].volume(value);
  }

  getAnimPath(){
    this.questionsService.getArchetypes().subscribe(archetypesList => {
        let archetype =  archetypesList.filter(item => item.name === this.profileResult[0].name)
        let thisPath = archetype[0].backAnim;
        return thisPath;
      });
  }

  setFinalResults() {
    this.profileResult = this.questionsService.getProfileResult();
    this.resultProfile = JSON.parse(sessionStorage.getItem('final-result'));
    this.image = this.resultProfile[0].image;
    this.backColor = this.resultProfile[0].gradient;
    this.fontColor = this.resultProfile[0].font;
    this.backImage = this.resultProfile[0].backImage;
    this.result_graphic = this.resultProfile[0].result;
    this.userQuestions = JSON.parse(sessionStorage.getItem('questionsList'));
    this.data = JSON.parse(sessionStorage.getItem('graph-data'));
    this.dataColor = JSON.parse(sessionStorage.getItem('graph-color'));
    this.fontColor = JSON.parse(sessionStorage.getItem('graph-font'));
    GoogleCharts.load(this.drawChart.bind(this, this.data, this.dataColor, this.fontColor));

    this.backAnimationConfig = JSON.parse(sessionStorage.getItem('backAnimation'));
    this.ending = this.userQuestions.ending;

    let userData = JSON.parse(sessionStorage.getItem('user')),
    answeredFormData = {
      "userId": /* userData.id || */ "dkaimoti2",
      "user": /* userData.name || */ "Diego2",
      "companyId": "Nuvi",
      "archetype": this.profileResult[0].name,
      "answers": JSON.parse(sessionStorage.getItem('answeredForm')),
      "resultData": this.data
    }

  //   this.questionsService.saveResults(answeredFormData)
  //     .subscribe(response => console.log(response))
  }

  drawChart(data, dataColor, fontColor) {
    console.log(data)
    let dataJson = GoogleCharts.api.visualization.arrayToDataTable(data);
    //let dataJson = data;
    var options = {
      backgroundColor: 'transparent',
      pieSliceBorderColor: 'transparent',
      pieHole: 0.5,
      pieSliceText: 'label',
      pieSliceTextStyle: {
        fontName: 'proxima_novasemibold',
        color: 'white'
      },
      colors: dataColor,
      legend: 'none',
      pieStartAngle: 180,
      slices: {  0: { offset: 0.08}},
      tooltip: {ignoreBounds: true, text: 'percentage', trigger: 'focus', textStyle: { fontName: 'proxima_novasemibold', color: fontColor}}
    };
    var chart = new GoogleCharts.api.visualization.PieChart(document.getElementById('googleGraphics'));

    chart.draw(dataJson, options);
  }

  seeResult(){
    this.iconNuvi = 'void'
    this.profileTitleState = 'void'
    this.profileArchetypeState = 'void'
    this.profileMessageState = 'void'
    this.imageState = 'void'
    this.bgAnim = 'void'
    this.progressMessage = 'ready'
    this.logoNuvi = 'ready'
    this.bgImage = 'ready'
    this.googleGraphics = 'ready'
    this.graphics = 'ready'
    this.logo.play();
  }

}

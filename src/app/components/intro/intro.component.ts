import { Component, OnInit, HostListener, ElementRef, Renderer2, ViewChild, Injectable } from '@angular/core';
import { trigger, state, style, transition, animate, keyframes } from '@angular/animations'
import { Router } from '@angular/router';
import Player from '@vimeo/player';
import { slideInOutAnimation } from '../../animations/slide-in-out.animation'
import { QuestionsService } from '../../services/questions.service';
import { StepperService } from '../../services/stepper.service';
import { debounce } from '../../animations/debounce.decorator';

@Component({
  selector: 'app-intro',
  templateUrl: './intro.component.html',
  styleUrls: ['./intro.component.scss'],
  animations: [slideInOutAnimation,
    trigger('dropIntro', [
      state('void', style({ opacity: 1})),
      state('ready', style({ opacity: 0})),
      state('fade', style({ opacity: 1})),
      transition('void => ready', [
        style({
          opacity: 1
        }),
        animate('1000ms 100ms ease-in-out')
      ]),
      transition('ready => fade', [
        style({
          opacity: 0
        }),
        animate('1000ms 100ms ease-in-out')
      ]),
      transition('fade => void', [
        style({
          opacity: 0
        }),
        animate('1000ms 100ms ease-in-out')
      ]),
    ]),
    trigger('nextIcon', [
      state('void', style({ opacity: 0, 'z-index': 100})),
      state('ready', style({ opacity: 1, 'z-index': 0})),
      transition('void => ready', [
        style({
          opacity: 1
        }),
        animate('1000ms 1000ms ease-in-out')
      ]),
      transition('ready => void', [
        style({
          opacity: 0
        }),
        animate('1000ms 1000ms ease-in-out')
      ]),
    ]),
  ],
  host: {'[@slideInOutAnimation]': ''}
})
export class IntroComponent implements OnInit {

    @ViewChild('introVideo') introVideo: ElementRef;
    @ViewChild('backIntro') backIntro: ElementRef;
    public animationConfigSkip: Object
    private skip: any;
    public animationConfigNext: Object
    private next: any;
    public animationConfigStart: Object
    private start: any;
    player: any;
    dropIntro = 'void'
    userQuestions: any

    constructor(
      private router: Router,
      private renderer: Renderer2,
      private elementRef: ElementRef,
      private stepperService: StepperService,
      private questionsService: QuestionsService
    ) {
          this.animationConfigSkip = {
              path: 'assets/anim-json/04_iconStart_A_loop.json',
              autoplay: false,
              loop: false
          };
          this.animationConfigNext = {
              path: 'assets/anim-json/04_iconStart_A_loop.json',
              autoplay: true,
              loop: true
          };
          this.animationConfigStart = {
              path: 'assets/anim-json/04_iconStart_B.json',
              autoplay: true,
              loop: false
          };
      }

    ngOnInit(){
      this.stepperService.setImages();
      this.getQuestionList();

      setTimeout(() =>{
          this.dropIntro = 'ready';
      }, 2500);

      this.player = new Player(this.introVideo.nativeElement);
      this.player.play();
      this.player.currentData = true;

      this.player.on('timeupdate', function(data) {
        if (data.percent >= 0.056 && data.percent <= 0.400 ) {
          let skipDiv = <HTMLElement>document.querySelector('#skipDiv');
          skipDiv.style.opacity = '1';
          skipDiv.style['pointer-events'] = 'auto';
        }
        if (data.percent >= 0.535 && this.currentData) {
          this.currentData = false
          let nextDiv = <HTMLElement>document.querySelector('#nextDiv');
          nextDiv.style.opacity = '1';
          nextDiv.style['z-index'] = 100;
          let skipDiv = <HTMLElement>document.querySelector('#skipDiv');
          skipDiv.style.opacity = '0';
          skipDiv.style['z-index'] = 0;
          skipDiv.style['pointer-events'] = 'none';
          let nextButton = <HTMLElement>document.querySelector('#nextButton');
          nextButton.style.cursor = 'pointer';
          nextButton.style['pointer-events'] = 'auto';
          this.pause();
        }
        if (data.percent >= 0.99) {
          let skipDiv = <HTMLElement>document.querySelector('#skipDiv');
          skipDiv.style.opacity = '0';
          skipDiv.style['pointer-events'] = 'none';
          let startDiv = <HTMLElement>document.querySelector('#startDiv');
          startDiv.style.opacity = '1';
          startDiv.style['z-index'] = 100;
          startDiv.style['background-color'] = '#DE0048';
          let startButton = <HTMLElement>document.querySelector('#startButton');
          startButton.style.cursor = 'pointer';
          startButton.style['pointer-events'] = 'auto';
        }
      });
    }

    handleSkip(anim: any) {
      this.skip = anim;
    }

    handleNext(anim: any) {
      this.next = anim;
    }

    handleStart(anim: any) {
      this.start = anim;
    }

    proceed(){
      let nextDiv = <HTMLElement>document.querySelector('#nextDiv');
      nextDiv.style.opacity = '0';
      nextDiv.style['z-index'] = 0;
      let nextButton = <HTMLElement>document.querySelector('#nextButton');
      nextButton.style.cursor = 'auto';
      nextButton.style['pointer-events'] = 'none';
      let skipDiv = <HTMLElement>document.querySelector('#skipDiv');
      skipDiv.style.opacity = '1';
      skipDiv.style['pointer-events'] = 'auto';
      skipDiv.style['z-index'] = 500;
      this.player.play();
    }

    skipIt(){
      let skipDiv = <HTMLElement>document.querySelector('#skipDiv');
      skipDiv.style.opacity = '0';
      skipDiv.style['z-index'] = 0;
      skipDiv.style['pointer-events'] = 'none';
      let startDiv = <HTMLElement>document.querySelector('#startDiv');
      startDiv.style.opacity = '1';
      startDiv.style['z-index'] = 100;
      startDiv.style['background-color'] = '#DE0048';
      let startButton = <HTMLElement>document.querySelector('#startButton');
      startButton.style.cursor = 'pointer';
      startButton.style['pointer-events'] = 'auto';
      this.player.pause();
    }

    startRouter(){
      let startButton = <HTMLElement>document.querySelector('#startButton');
      startButton.style.cursor = 'auto';
      startButton.style['pointer-events'] = 'none';
      startButton.style.opacity = '0';
      let startSpan = <HTMLElement>document.querySelector('#startSpan');
      startSpan.style.opacity = '0';

      let nextQuestion = this.userQuestions.questions.filter((question) =>{
        if(question.step == 1){
          return question
        }
      });

      sessionStorage.setItem('currentStep', '1')
        if(this.stepperService.detectmob()){
          this.router.navigate([nextQuestion[0].templateMobile])
        }else{
          if(this.stepperService.detectIe()){
            this.router.navigate([nextQuestion[0].templateIE])
          }else{
            this.router.navigate([nextQuestion[0].template])
          }
        }
    }

    getQuestionList(){
      this.questionsService.getQuestions()
        .subscribe(res => {
          this.userQuestions = res.PT;
        })
    }
}

import { Component, OnInit, HostListener, ElementRef, ViewChild } from '@angular/core';
import { slideInOutAnimation } from '../../animations/slide-in-out.animation';
import { trigger, state, style, transition, animate, keyframes } from '@angular/animations';
import { Router } from '@angular/router';
import { Question } from '../../models/question.model';
import { StepperService } from '../../services/stepper.service';
import { QuestionsService } from '../../services/questions.service';
import { TieBreakerStates } from './tie-breaker.model';

@Component({
  selector: 'app-tie-breaker',
  templateUrl: './tie-breaker.component.html',
  styleUrls: ['./tie-breaker.component.scss'],
  animations: [slideInOutAnimation,
    trigger('question', [
      state('void', style({ opacity: 0})),
      state('ready', style({ opacity: 1})),
      transition('void => ready', [
        style({
          opacity: 0,
          transform: 'translate(0, 50px)'
        }),
        animate('500ms 1500ms ease-in-out')
      ]),
    ]),
    trigger('firstOption', [
      state('void', style({ opacity: 0})),
      state('ready', style({ opacity: 1})),
      state('fade', style({ opacity: 0.5, transform: 'translate(50px, 0)'})),
      transition('void => ready', [
        style({
          opacity: 0,
          transform: 'translate(1000px, 0)'
        }),
        animate('500ms 1700ms ease-in-out')
      ]),
      transition('ready => fade', [
        style({
          opacity: 0.5,
          transform: 'translate(0, 0)'
        }),
        animate('500ms 0s ease-in-out')
      ]),
    ]),
    trigger('secondOption', [
      state('void', style({ opacity: 0})),
      state('ready', style({ opacity: 1})),
      state('fade', style({ opacity: 0.5, transform: 'translate(50px, 0)'})),
      transition('void => ready', [
        style({
          opacity: 0,
          transform: 'translate(1000px, 0)'
        }),
        animate('500ms 1850ms ease-in-out')
      ]),
      transition('ready => fade', [
        style({
          opacity: 0.5,
          transform: 'translate(0, 0)'
        }),
        animate('500ms 0s ease-in-out')
      ]),
    ]),
    trigger('thirdOption', [
      state('void', style({ opacity: 0})),
      state('ready', style({ opacity: 1})),
      state('fade', style({ opacity: 0.5, transform: 'translate(50px, 0)'})),
      transition('void => ready', [
        style({
          opacity: 0,
          transform: 'translate(1000px, 0)'
        }),
        animate('500ms 2000ms ease-in-out')
      ]),
      transition('ready => fade', [
        style({
          opacity: 0.5,
          transform: 'translate(0, 0)'
        }),
        animate('500ms 0s ease-in-out')
      ]),
    ]),
    trigger('forthOption', [
      state('void', style({ opacity: 0})),
      state('ready', style({ opacity: 1})),
      state('fade', style({ opacity: 0.5, transform: 'translate(50px, 0)'})),
      transition('void => ready', [
        style({
          opacity: 0,
          transform: 'translate(1000px, 0)'
        }),
        animate('500ms 2150ms ease-in-out')
      ]),
      transition('ready => fade', [
        style({
          opacity: 0.5,
          transform: 'translate(0, 0)'
        }),
        animate('500ms 0s ease-in-out')
      ]),
    ]),
    trigger('fifthOption', [
      state('void', style({ opacity: 0})),
      state('ready', style({ opacity: 1})),
      state('fade', style({ opacity: 0.5, transform: 'translate(50px, 0)'})),
      transition('void => ready', [
        style({
          opacity: 0,
          transform: 'translate(1000px, 0)'
        }),
        animate('500ms 2300ms ease-in-out')
      ]),
      transition('ready => fade', [
        style({
          opacity: 0.5,
          transform: 'translate(0, 0)'
        }),
        animate('500ms 0s ease-in-out')
      ]),
    ]),
    trigger('selectOption', [
      state('ready', style({ opacity: 1 })),
      state('void', style({ opacity: 0 })),
      transition('void => ready', [
        animate('4s 2500ms', keyframes([
          style({opacity: '0', transition: '1s ease', offset: 0 }),
          style({opacity: '0.5', transition: '1s ease', offset: 0.1 }),
          style({opacity: '1', transition: '1s ease', offset: 0.2 }),
          style({opacity: '0.5', transition: '1s ease', offset: 0.3 }),
          style({opacity: '1', transition: '1s ease', offset: 0.4 }),
          style({opacity: '0.5', transition: '1s ease', offset: 0.5 }),
          style({opacity: '1', transition: '1s ease', offset: 0.6 }),
          style({opacity: '0.5', transition: '1s ease', offset: 0.7 }),
          style({opacity: '1', transition: '1s ease', offset: 0.8 }),
          style({opacity: '0.5', transition: '1s ease', offset: 0.9 }),
          style({opacity: '1', transition: '1s ease', offset: 1 }),
        ]))
      ]),
      transition('ready => void', [
        style({ opacity: 0 }),
        animate('600ms 0s ease-in-out')
      ])
    ]),
  ],
  host: {'[@slideInOutAnimation]': ''}
})
export class TieBreakerComponent implements OnInit {

  backgroundAnimationConfig: object
  questionState: string = "void"

  optionState = {
    first:'void',
    second:'void',
    third:'void',
    forth:'void',
    fifth:'void'
   } as TieBreakerStates

  selectOptionState: string = "void"
  enableMovement: boolean = false
  questionObject: Question
  userQuestions: any
  scoreBoard: any
  firstAnswer = {options: {answer: '', archetype: ''}, enabled   : false}
  secondAnswer = {options: {answer: '', archetype: ''}, enabled   : false}
  thirdAnswer = {options: {answer: '', archetype: ''}, enabled   : false}

  @ViewChild('background') animationContainer: ElementRef;

  constructor(
    private router: Router,
    private stepperService: StepperService,
    private questionsService: QuestionsService
  ) {
    this.getCurrentQuestion()

  }

  ngOnInit() {
    this.scoreBoard = this.questionsService.getProfileResult();
    this.userQuestions = JSON.parse(sessionStorage.getItem('questionsList'))
    this.getTiedList();
    this.getCurrentQuestion()


    setTimeout(() => {
      this.questionState = "ready"

      this.selectOptionState = 'ready'
      this.enableMovement = true
    }, 50)

    setTimeout(() => {
        this.optionState.first = "ready"
        this.optionState.second= "ready"
        this.optionState.third = "ready"
        this.optionState.forth = "ready"
        this.optionState.fifth = "ready"
    }, 300)

  }

  selectOption(selected, option) {
    for (const key in this.optionState) {
      if(key == option){
        this.optionState[key] = 'ready'
      } else {
        this.optionState[key] = 'fade'
      }
    }

    this.questionsService.setQuestionsAnswered(this.questionObject.step, selected)

    setTimeout(() => {
      this.goToNextStep()
    }, 1000)
  }

  getCurrentQuestion(){
    this.questionObject = this.stepperService.getCurrentQuestion()
  }

  goToNextStep() {
    this.selectOptionState = 'void'
    let nextQuestion = this.stepperService.getNextQuestion()
    this.router.navigate(['pre-result'])
  }

  getTiedList(){
    this.firstAnswer.options.answer = this.questionObject.options.filter(item => item.archetype === this.scoreBoard[0].name)[0].answer
    this.firstAnswer.options.archetype = this.questionObject.options.filter(item => item.archetype === this.scoreBoard[0].name)[0].archetype
    this.firstAnswer.enabled = true;

    this.secondAnswer.options.answer = this.questionObject.options.filter(item => item.archetype === this.scoreBoard[1].name)[0].answer
    this.secondAnswer.options.archetype = this.questionObject.options.filter(item => item.archetype === this.scoreBoard[1].name)[0].archetype
    this.secondAnswer.enabled = true;

    if(this.scoreBoard[0].value === this.scoreBoard[1].value && this.scoreBoard[1].value === this.scoreBoard[2].value){
      this.thirdAnswer.options.answer = this.questionObject.options.filter(item => item.archetype === this.scoreBoard[2].name)[0].answer
      this.thirdAnswer.options.archetype = this.questionObject.options.filter(item => item.archetype === this.scoreBoard[2].name)[0].archetype
      this.thirdAnswer.enabled = true;
    }
  }
}

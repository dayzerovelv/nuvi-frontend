export interface TieBreakerStates {
    first: string
    second: string
    third: string
    forth: string
    fifth: string
}

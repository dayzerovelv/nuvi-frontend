import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionTwoOptionsImageComponent } from './question-two-options-image.component';

describe('QuestionTwoOptionsImageComponent', () => {
  let component: QuestionTwoOptionsImageComponent;
  let fixture: ComponentFixture<QuestionTwoOptionsImageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuestionTwoOptionsImageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionTwoOptionsImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { trigger, state, style, transition, animate, keyframes } from '@angular/animations';
import { slideInOutAnimation } from '../../animations/slide-in-out.animation';
import { Router } from '@angular/router';
import { Question } from '../../models/question.model';
import { StepperService } from '../../services/stepper.service';
import { QuestionsService } from '../../services/questions.service';

@Component({
  selector: 'app-question-two-options-image-circle',
  templateUrl: './question-two-options-image-circle.component.html',
  styleUrls: ['./question-two-options-image-circle.component.scss'],
  animations: [slideInOutAnimation,
    trigger('dropIntro', [
      state('void', style({ opacity: 1},)),
      state('ready', style({ opacity: 1})),
      transition('void => ready', [
        style({ 
        }),
        animate('1000ms 1200ms ease-in-out')
      ]),
      transition('ready => void', [
        style({ 
          opacity: 1,
          
        }),
        animate('1000ms 1200ms ease-in-out')
      ]),
    ]),     
    trigger('back', [
      state('void', style({ opacity: 1, transform: 'translate(400px,400px)',})),
      state('ready', style({ opacity: 1, transform: 'translate(0px,0px)'})),
      transition('void => ready', [
        style({ 
          transform: 'translate(400px, 400px)',
        }),
        animate('500ms 300ms ease-in-out')
      ]),
      transition('ready => void', [
        style({ 
          opacity: 1,
          
        }),
        animate('1000ms 1200ms cubic-bezier(.18,.6,.42,.98)')
      ]),
    ]),     
    trigger('front', [
      state('void', style({ opacity: 1, transform: 'translate(400px,400px)',})),
      state('ready', style({ opacity: 1, transform: 'translate(0px,0px)'})),
      transition('void => ready', [
        style({ 
          transform: 'translate(400px, 400px)',
        }),
        animate('500ms 300ms ease-in-out')
      ]),
      transition('ready => void', [
        style({ 
          opacity: 1,
          
        }),
        animate('1000ms 1200ms cubic-bezier(.18,.6,.42,.98)')
      ]),
    ]),     
    trigger('question', [
      state('void', style({ opacity: 0, transform: 'translate(0, -200px)',})),
      state('show', style({ opacity: 1, 'font-size': '2.5rem'})),
      state('ready', style({ opacity: 1, transform: 'translate(0, -200px)'})),
      transition('void => show', [
        style({ 
          opacity: 0,
          transform: 'translate(0,100px)',
        }),
        animate('500ms 0s ease-in-out')
      ]),
      transition('show => ready', [
        style({ 
          opacity: 1,
        }),
        animate('500ms 0ms ease-in-out')
      ]),
      transition('ready => void', [
        style({ 
          opacity: 1,
        }),
        animate('800ms 700ms ease-in-out')
      ]),
    ]),
    trigger('firstOption', [
      state('void', style({ opacity: 0})),
      state('ready', style({ opacity: 1})),
      state('hover', style({ opacity: 1 })),
      transition('void => ready', [
        style({ 
          opacity: 0,
          transform: 'translate(0,200px)'
        }),
        animate('300ms 100ms ease-in-out')
      ]),
      transition('ready => hover', [
        style({
          opacity: 1
        }),
        animate('300ms 0s ease-in-out')
      ]),
      transition('ready => void', [
        style({ 
          opacity: 1,
        }),
        animate('800ms 600ms ease-in-out')
      ]),
    ]),
    trigger('secondOption', [
      state('void', style({ opacity: 0})),
      state('ready', style({ opacity: 1 })),
      state('hover', style({ opacity: 1})),
      transition('void => ready', [
        style({ 
          opacity: 0,
          transform: 'translate(0,200px)'
        }),
        animate('300ms 100ms ease-in-out')
      ]),
      transition('ready => hover', [
        style({
          opacity: 1
        }),
        animate('300ms 0s ease-in-out')
      ]),
      transition('ready => void', [
        style({ 
          opacity: 1,
        }),
        animate('800ms 600ms ease-in-out')
      ]),
    ]),
    trigger('feedbackText', [
      state('ready', style({ opacity: 1 })),
      transition('void => ready', [
        style({ opacity: 0 }),
        animate('300ms 0s ease-in-out')
      ])
    ]),
    trigger('selectOption', [
      state('ready', style({ opacity: 1 })),
      transition('void => ready', [
        animate('4s', keyframes([
          style({opacity: '0', transition: '1s ease', offset: 0 }),
          style({opacity: '0.5', transition: '1s ease', offset: 0.1 }),
          style({opacity: '1', transition: '1s ease', offset: 0.2 }),
          style({opacity: '0.5', transition: '1s ease', offset: 0.3 }),
          style({opacity: '1', transition: '1s ease', offset: 0.4 }),
          style({opacity: '0.5', transition: '1s ease', offset: 0.5 }),
          style({opacity: '1', transition: '1s ease', offset: 0.6 }),
          style({opacity: '0.5', transition: '1s ease', offset: 0.7 }),
          style({opacity: '1', transition: '1s ease', offset: 0.8 }),
          style({opacity: '0.5', transition: '1s ease', offset: 0.9 }),
          style({opacity: '1', transition: '1s ease', offset: 1 }),
        ]))
      ]),
      transition('ready => void', [
        style({ opacity: 0 }),
        animate('600ms 600ms ease-in-out')
      ])
    ]),
  ],
  host: { '[@slideInOutAnimation]': '' }
})
export class QuestionTwoOptionsImageCircleComponent implements OnInit {

  anim: any
  backgroundAnimationConfig: object
  dropIntro: string = 'void'
  back: string = 'void'
  front: string = 'void'
  questionState: string = 'void'
  firstOptionState: string = 'void'
  secondOptionState: string = 'void'
  feedbackTextState: string = 'void'
  selectOptionState: string = 'void'
  imageUrl: string
  questionObject: Question
  enableMovement: boolean = false
  userQuestions: any


  constructor(
    private router: Router,
    private stepperService: StepperService,
    private questionsService: QuestionsService
  ) {

    this.getCurrentQuestion()

    this.backgroundAnimationConfig = {
      path: this.questionObject.templateUrl,
      autoplay: true,
      loop: false,
      rendererSettings: {
        className: 'svg-ie'
      }
    }

  }

  ngOnInit() {

    setTimeout(() => {
      this.dropIntro = 'ready'
      this.questionState = 'show'
    }, 1800)

    this.userQuestions = JSON.parse(sessionStorage.getItem('questionsList'))
    setTimeout(() => {
      this.back = 'ready'
      this.front = 'ready'
    }, 2200)

    setTimeout(() => {
      this.questionState = 'ready'
      this.firstOptionState = 'ready'
      this.secondOptionState = 'ready'
    }, 2500)


    setTimeout(() => {
      this.selectOptionState = 'ready'
      this.enableMovement = true
    }, 4500)

    this.getCurrentQuestion();
  }
  
  selectOptionA(selected) {
    this.secondOptionState = 'hover'
    this.clearOptionsAndSetFeedback(selected)
  }

  selectOptionB(selected) {
    this.firstOptionState = 'hover'
    this.clearOptionsAndSetFeedback(selected)
  }

  clearOptionsAndSetFeedback(selected){
    this.dropIntro = 'void'
    this.back = 'void'
    this.front = 'void'
    this.questionState = 'void'
    this.firstOptionState = 'void'
    this.secondOptionState = 'void'
    this.selectOptionState = 'void'
    this.questionsService.setQuestionsAnswered(this.questionObject.step, selected)
    
    setTimeout(() => {
      this.goToNextStep()
    }, 2000)
  }

  getCurrentQuestion() {
    this.questionObject = this.stepperService.getCurrentQuestion()
  }

  goToNextStep() {
    let nextQuestion = this.stepperService.getNextQuestion()
    this.router.navigate([nextQuestion.template])
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionTwoOptionsImageCircleComponent } from './question-two-options-image-circle.component';

describe('QuestionTwoOptionsImageCircleComponent', () => {
  let component: QuestionTwoOptionsImageCircleComponent;
  let fixture: ComponentFixture<QuestionTwoOptionsImageCircleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuestionTwoOptionsImageCircleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionTwoOptionsImageCircleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

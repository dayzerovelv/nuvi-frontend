import { Location } from '@angular/common';
import { Component, OnInit} from '@angular/core';
import { Question } from '../../models/question.model';
import { StepperService } from '../../services/stepper.service';
import { QuestionsService } from '../../services/questions.service';
import { Router, NavigationEnd } from '@angular/router';
import { trigger, state, style, transition, animate, keyframes } from '@angular/animations';

@Component({
  selector: 'app-progress-bar',
  templateUrl: './progress-bar.component.html',
  styleUrls: ['./progress-bar.component.scss'],
  animations: [
    trigger('progressBar', [
      state('void', style({ opacity: 0})),
      state('ready', style({ opacity: 1})),
      transition('void => ready', [
        style({
          opacity: 0,
        }),
        animate('100ms 0ms cubic-bezier(0, 0, 0.20, 1)')
      ]),
      transition('ready => void', [
        style({
          opacity: 1,
        }),
        animate('100ms 0ms cubic-bezier(.62,.22,.19,.88)')
      ]),
    ]),
    trigger('progressBarMessage', [
      state('void', style({ opacity: 0})),
      state('ready', style({ opacity: 1})),
      transition('void => ready', [
        style({
          opacity: 0,
        }),
        animate('500ms 0ms cubic-bezier(0, 0, 0.20, 1)')
      ]),
      transition('ready => void', [
        style({
          opacity: 1,
        }),
        animate('500ms 0ms cubic-bezier(0, 0, 0.20, 1)')
      ]),
    ]),
    trigger('backButton', [
      state('void', style({ opacity: 1,})),
      state('ready', style({ opacity: 0})),
      transition('void => ready', [
        style({
          opacity: 0,
        }),
        animate('100ms 0ms cubic-bezier(.62,.22,.19,.88)')
      ]),
      transition('ready => void', [
        style({
          opacity: 1,
        }),
        animate('100ms 0ms cubic-bezier(.62,.22,.19,.88)')
      ]),
    ]),
  ],
})
export class ProgressBarComponent implements OnInit {

  currentCachedStep: number
  currentStep: number
  questionsCache: any
  sessionsNumber: any
  backButton: string = 'void'
  circleList: Array<any> = []
  progressBarState: string = 'void'
  progressBarMessageState: string = 'void'
  barHeight: string
  currentPosition: string
  progressMessage: string
  messagePosition: string
  userQuestions: any
  routerUrl: any
  isComponent: any
  templateArray = ['4sUA9LCWtZNEftCXWDcg9fT4Cg5X6Ca50OjLOAs9C3kntFN3qf',
'KFwbHRvlLmfRex5zHl3mx2qKHDOVrCFctLBjcqtBDjEcpQz3Hm',
'd9TxqMOgBhB9L2w6uc2qKSUwjvwKSuxMwW2OZaM0PwV6iT6Ab8',
'FaaaTbebnES4xj6VKJLss75NTeX1k42LDUZmmYDC9wjltCB8Mw',
'xlHiM9xiHxMJv53OYZM4E8sn7hCRC6J8lbrgvuuJkfR7pkZngp']

  constructor(
    private router: Router,
    private location: Location,
    private stepperService: StepperService,
    private questionsService: QuestionsService
  ) {
    router.events.subscribe((val) => {
      if(this.checkTemplate()){
        if(val instanceof NavigationEnd){
          this.backButton = 'void'
          this.progressBarState = 'ready'
          this.updateButton()
          this.routerUrl = val
          this.currentStep = parseInt(sessionStorage.getItem('currentStep')) || 0
          this.questionsCache = val.url != '/' ? JSON.parse(sessionStorage.getItem('questionsList')) : ''
          this.sessionsNumber = this.questionsCache ? this.questionsCache.questions.length / 4 : undefined
          this.currentCachedStep = parseInt(sessionStorage.getItem('currentStep'))
          this.progressMessage = ''

          this.barHeight = this.questionsCache ? `${this.questionsCache.questions.length * 5}px` : ''
          this.currentPosition = `${(this.currentStep -1) * 5}px`
          this.messagePosition = `${((this.currentStep -1) * 5) - 95}px`

          if(this.currentCachedStep > 0 && (this.currentCachedStep % 4 === 0)) {
            this.currentStep++
          }

          for(let i = 0; i <= this.sessionsNumber; i++) {
            this.circleList.push(i)
          }
        }
      }else {
        this.backButton = 'ready'
        this.progressBarState = 'void'
      }
    });
  }

  ngOnInit() {
    this.updateButton()
    this.currentStep = parseInt(sessionStorage.getItem('currentStep')) || 0;
  }

  updateButton(){
    this.isComponent = this.templateArray.includes(window.location.href.split('/').pop());
  }

  checkTemplate(){
    return this.templateArray.includes(window.location.href.split('/').pop());
  }

  back() {
    // this.location.back()
    var previousQuestion = this.stepperService.getPreviousQuestion()
    sessionStorage.setItem('currentStep', previousQuestion[0].step)
    if(this.stepperService.detectmob()){
      this.router.navigate([previousQuestion[0].templateMobile])
    }else{
      if(this.stepperService.detectIe()){
        this.router.navigate([previousQuestion[0].templateIE])
      }else{
        this.router.navigate([previousQuestion[0].template])
      }
    }
  }

}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatSnackBar,MatSnackBarConfig } from '@angular/material';
import { QuestionsService } from '../../services/questions.service';
import { StepperService } from '../../services/stepper.service';
import { slideInOutAnimation } from '../../animations/slide-in-out.animation';
import { trigger, state, style, transition, animate, keyframes } from '@angular/animations';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  animations: [slideInOutAnimation,
  ],
  host: {'[@slideInOutAnimation]': ''}
})
export class LoginComponent implements OnInit {

  animationConfig: object;
  dropIntro: string = 'void';
  timeOut: any;
  position = Math.floor(Math.random() * 3);
  currentQuestion: any;
  split: any;
  inputUserName: String = '';
  inputEmail: String = '';

  constructor(
    private router: Router, public snackBar: MatSnackBar) {
  }

  ngOnInit() {
  }

  auth(){
    let input = document.getElementById("userEmail");
    this.inputEmail = input.value
    console.log(this.inputEmail)

     if (this.inputEmail === '') {
      this.router.navigate(['OTXVm5HsrnU5z8MBnabJWCQnYYNq5qM1s2VmovNzIU5D0waFoY'])
     }else {
       this.snackBar.open('Usuário não existe.', 'OK', {
         duration: 2000,
         extraClasses: 'snack-style'
       });
     }
  }
}


import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionTwoOptionsImageTrapezoidsComponent } from './question-two-options-image-trapezoids.component';

describe('QuestionTwoOptionsImageComponent', () => {
  let component: QuestionTwoOptionsImageTrapezoidsComponent;
  let fixture: ComponentFixture<QuestionTwoOptionsImageTrapezoidsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuestionTwoOptionsImageTrapezoidsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionTwoOptionsImageTrapezoidsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

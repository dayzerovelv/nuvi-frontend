import { Component, OnInit } from '@angular/core';
import { trigger, state, style, transition, animate, keyframes } from '@angular/animations';
import { slideInOutAnimation } from '../../animations/slide-in-out.animation';
import { Router } from '@angular/router';
import { Question } from '../../models/question.model';
import { StepperService } from '../../services/stepper.service';
import { QuestionsService } from '../../services/questions.service';

@Component({
  selector: 'app-question-two-options-image-trapezoids',
  templateUrl: './question-two-options-image-trapezoids.component.html',
  styleUrls: ['./question-two-options-image-trapezoids.component.scss'],
  animations: [slideInOutAnimation,
    trigger('dropIntro', [
      state('void', style({ transform:'translate(-20%) skewX(45deg)'},)),
      state('ready', style({ transform:'translate(-90%) skewX(45deg)'})),
      transition('void => ready', [
        style({transform:'skewX(45deg) translate(-20%)'
        }),
        animate('1000ms 600ms ease-in-out')
      ]),
      transition('ready => void', [
        style({transform:'skewX(45deg) translate(-90%)'
        }),
        animate('1000ms 600ms ease-in-out')
      ]),
    ]),
    trigger('arrowAcontainer', [
      state('void', style({ opacity: 0})),
      state('ready', style({ opacity: 1})),
      transition('void => ready', [
        style({
          opacity: 1
        }),
        animate('1000ms 500ms ease-in-out')
      ]),
      transition('ready => void', [
        style({
          opacity: 1
        }),
        animate('1000ms 100ms ease-in-out')
      ]),
    ]),
    trigger('arrowBcontainer', [
      state('void', style({ opacity: 0})),
      state('ready', style({ opacity: 1})),
      transition('void => ready', [
        style({
          opacity: 1
        }),
        animate('1000ms 500ms ease-in-out')
      ]),
      transition('ready => void', [
        style({
          opacity: 0
        }),
        animate('1000ms 100ms ease-in-out')
      ]),
    ]),
    trigger('option1polygon', [
      state('void', style({ opacity: 1, transform:'translate(50%)'},)),
      state('ready', style({ opacity: 1, transform:'translate(0%)'})),
      transition('void => ready', [
        style({
          transform:'translate(50%)'
        }),
        animate('1000ms 400ms ease-in-out')
      ]),
      transition('ready => void', [
        style({
          opacity: 1,
          transform:'translate(0%)'
        }),
        animate('1000ms 600ms ease-in-out')
      ]),
    ]),
    trigger('option2polygon', [
      state('void', style({ opacity: 1, transform:'translate(50%)'},)),
      state('ready', style({ opacity: 1, transform:'translate(0%)'})),
      transition('void => ready', [
        style({
          opacity: 1,
          transform:'translate(50%)'
        }),
        animate('1000ms 500ms ease-in-out')
      ]),
      transition('ready => void', [
        style({
          opacity: 1,
          transform:'translate(0%)'
        }),
        animate('1000ms 500ms ease-in-out')
      ]),
    ]),
    trigger('question', [
      state('void', style({ opacity: 0,})),
      state('show', style({ opacity: 1})),
      state('ready', style({ opacity: 1})),
      transition('void => show', [
        style({
          opacity: 0,
        }),
        animate('400ms 500ms cubic-bezier(.62,.22,.19,.88)')
      ]),
      transition('show => ready', [
        style({
          opacity: 1,
        }),
        animate('800ms 500ms cubic-bezier(.62,.22,.19,.88)')
      ]),
      transition('ready => void', [
        style({
          opacity: 1,
        }),
        animate('800ms 700ms cubic-bezier(0, 0, 0.20, 1)')
      ]),
    ]),
    trigger('firstOption', [
      state('void', style({ opacity: 0})),
      state('ready', style({ opacity: 1})),
      state('hover', style({ opacity: 0.5 })),
      transition('void => ready', [
        style({
          opacity: 0,
          transform: 'translate(0,100px)'
        }),
        animate('500ms 500ms cubic-bezier(0, 0, 0.20, 1)')
      ]),
      transition('ready => hover', [
        style({
          opacity: 0.5
        }),
        animate('300ms 500ms ease-in-out')
      ]),
      transition('ready => void', [
        style({
          opacity: 1,
        }),
        animate('800ms 500ms cubic-bezier(0, 0, 0.20, 1)')
      ]),
    ]),
    trigger('secondOption', [
      state('void', style({ opacity: 0})),
      state('ready', style({ opacity: 1 })),
      state('hover', style({ opacity: 0.5})),
      transition('void => ready', [
        style({
          opacity: 0,
          transform: 'translate(0,100px)'
        }),
        animate('500ms 500ms cubic-bezier(0, 0, 0.20, 1)')
      ]),
      transition('ready => hover', [
        style({
          opacity: 0.5
        }),
        animate('300ms 500ms ease-in-out')
      ]),
      transition('ready => void', [
        style({
          opacity: 1,
        }),
        animate('800ms 500ms cubic-bezier(0, 0, 0.20, 1)')
      ]),
    ]),
  ],
  host: { '[@slideInOutAnimation]': '' }
})
export class QuestionTwoOptionsImageTrapezoidsComponent implements OnInit {
  backgroundAnimationConfig: object
  public arrowAnimationConfigA: Object
  public arrowAnimationConfigB: Object
  public trapezoidAnimationConfig: Object
  private arrowA: any;
  private arrowB: any;
  trapezoid: any;
  dropIntro: string = 'void'
  arrowAcontainer: string = 'void'
  arrowBcontainer: string = 'void'
  option1polygon: string = 'void'
  option2polygon: string = 'void'
  option1container: string = 'void'
  option2container: string = 'void'
  questionState: string = 'void'
  firstOptionState: string = 'void'
  secondOptionState: string = 'void'
  feedbackTextState: string = 'void'
  selectOptionState: string = 'void'
  imageUrl: string
  questionObject: Question
  enableMovement: boolean = false
  userQuestions: any
  imageList: any
  image1: string
  image2: string


  constructor(
    private router: Router,
    private stepperService: StepperService,
    private questionsService: QuestionsService
  ) {
    this.getCurrentQuestion()
    this.arrowAnimationConfigA = {
        path: 'assets/anim-json/00_buttonA_loop.json',
        autoplay: true,
        loop: true
    };
    this.arrowAnimationConfigB = {
        path: 'assets/anim-json/00_buttonB_loop.json',
        autoplay: true,
        loop: true
    };
    this.trapezoidAnimationConfig = {
        path: 'assets/anim-json/trapezoid.json',
        autoplay: false,
        loop: false
    };
  }

  ngOnInit() {
    this.userQuestions = JSON.parse(sessionStorage.getItem('questionsList'))
    this.questionObject = this.stepperService.getCurrentQuestion()
    this.stepperService.getImages().then((res)=>{
      this.imageList = res;
      let thisImage = this.imageList.filter(item => item.step === this.questionObject.step)
      this.image1 = thisImage[0].image1.src;
      this.image2 = thisImage[0].image2.src;
    })
    setTimeout(() => {
      this.arrowAcontainer = 'ready'
      this.arrowBcontainer = 'ready'
      this.option1polygon = 'ready'
      this.option2polygon = 'ready'
      this.option1container = 'ready'
      this.option2container = 'ready'
    }, 750)

    setTimeout(() => {
      this.trapezoid.playSegments([0, 200],true);
      this.questionState = 'show'
      this.firstOptionState = 'ready'
      this.secondOptionState = 'ready'
    }, 1500)

    setTimeout(() => {
      let option1 = <HTMLElement>document.querySelector('#option1');
      option1.style['pointer-events'] = 'auto';
      let option2 = <HTMLElement>document.querySelector('#option2');
      option2.style['pointer-events'] = 'auto';
    }, 2500)

    setTimeout(() => {
      this.selectOptionState = 'ready'
      this.enableMovement = true
    }, 3500)

  }

  handleArrowA(anim: any) {
    this.arrowA = anim;
  }

  handleArrowB(anim: any) {
    this.arrowB = anim;
  }

  handleTrapezoid(anim: any) {
    this.trapezoid = anim;
  }

  selectOptionA(selected) {
    this.secondOptionState = 'hover'
    this.arrowBcontainer = 'void';
    let option1 = <HTMLElement>document.querySelector('#option1');
    option1.style['pointer-events'] = 'none';
    let option2 = <HTMLElement>document.querySelector('#option2');
    option2.style['pointer-events'] = 'none';
    this.clearOptionsAndSetFeedback(selected)
  }

  selectOptionB(selected) {
    this.firstOptionState = 'hover'
    this.arrowAcontainer = 'void';
    let option1 = <HTMLElement>document.querySelector('#option1');
    option1.style['pointer-events'] = 'none';
    let option2 = <HTMLElement>document.querySelector('#option2');
    option2.style['pointer-events'] = 'none';
    this.clearOptionsAndSetFeedback(selected)
  }

  clearOptionsAndSetFeedback(selected){
      setTimeout(() =>{
      this.trapezoid.playSegments([350, 600],true);
      }, 100)

      setTimeout(() =>{
      this.option1polygon = 'void'
      this.option2polygon = 'void'
      this.questionState = 'void'
      this.firstOptionState = 'void'
      this.secondOptionState = 'void'
      this.selectOptionState = 'void'
      this.arrowAcontainer = 'void';
      this.arrowBcontainer = 'void';
      this.questionsService.setQuestionsAnswered(this.questionObject.step, selected)
    }, 300)

    setTimeout(() => {
      this.goToNextStep()
    }, 2000)
  }

  getCurrentQuestion() {
    this.questionObject = this.stepperService.getCurrentQuestion()
  }

  goToNextStep() {
    let nextQuestion = this.stepperService.getNextQuestion()
    if((nextQuestion.step - 1) % 4 === 0){
      this.router.navigate(['0OB1cztzwtmxwVQVlD00FPdocl1jeV8bZKUxtDbzb09J7FAgLq'])
    }else {
      if(this.stepperService.detectmob()){
        this.router.navigate([nextQuestion.templateMobile])
      }else{
        if(this.stepperService.detectIe()){
          this.router.navigate([nextQuestion.templateIE])
        }else{
          this.router.navigate([nextQuestion.template])
        }
      }
    }
  }
}

import { Component, OnInit } from '@angular/core';
import { trigger, state, style, transition, animate, keyframes } from '@angular/animations';
import { slideInOutAnimation } from '../../animations/slide-in-out.animation';
import { Router } from '@angular/router';
import { Question } from '../../models/question.model';
import { StepperService } from '../../services/stepper.service';
import { QuestionsService } from '../../services/questions.service';

@Component({
  selector: 'app-mobile',
  templateUrl: './mobile.component.html',
  styleUrls: ['./mobile.component.scss'],
  animations: [slideInOutAnimation,
    trigger('dropIntro', [
      state('void', style({ opacity: 1},)),
      state('ready', style({ opacity: 1})),
      transition('void => ready', [
        style({
        }),
        animate('1000ms 1200ms ease-in-out')
      ]),
      transition('ready => void', [
        style({
          opacity: 1
        }),
        animate('1000ms 1200ms ease-in-out')
      ]),
    ]),
    trigger('question', [
      state('void', style({ opacity: 0})),
      state('show', style({ opacity: 1, 'font-size': '1.4rem'})),
      state('ready', style({ opacity: 1, top: '10%'})),
      transition('void => show', [
        style({
          opacity: 0,
        }),
        animate('400ms 0s cubic-bezier(.62,.22,.19,.88)')
      ]),
      transition('show => ready', [
        style({
          opacity: 1,
        }),
        animate('800ms 600ms cubic-bezier(.62,.22,.19,.88)')
      ]),
      transition('ready => void', [
        style({
          opacity: 1,
        }),
        animate('800ms 700ms cubic-bezier(0, 0, 0.20, 1)')
      ]),
    ]),
    trigger('firstOption', [
      state('void', style({ opacity: 0})),
      state('ready', style({ opacity: 1})),
      state('hover', style({ opacity: 0.5 })),
      transition('void => ready', [
        style({
          opacity: 0,
          transform: 'translate(0,100px)'
        }),
        animate('500ms 900ms cubic-bezier(0, 0, 0.20, 1)')
      ]),
      transition('ready => hover', [
        style({
          opacity: 0.5
        }),
        animate('300ms 0s ease-in-out')
      ]),
      transition('ready => void', [
        style({
          opacity: 1,
        }),
        animate('800ms 600ms cubic-bezier(0, 0, 0.20, 1)')
      ]),
    ]),
    trigger('secondOption', [
      state('void', style({ opacity: 0})),
      state('ready', style({ opacity: 1 })),
      state('hover', style({ opacity: 0.5})),
      transition('void => ready', [
        style({
          opacity: 0,
          transform: 'translate(0,100px)'
        }),
        animate('500ms 1000ms cubic-bezier(0, 0, 0.20, 1)')
      ]),
      transition('ready => hover', [
        style({
          opacity: 0.5
        }),
        animate('300ms 0s ease-in-out')
      ]),
      transition('ready => void', [
        style({
          opacity: 1,
        }),
        animate('800ms 600ms cubic-bezier(0, 0, 0.20, 1)')
      ]),
    ]),
    trigger('feedbackText', [
      state('ready', style({ opacity: 1 })),
      transition('void => ready', [
        style({ opacity: 0 }),
        animate('300ms 0s ease-in-out')
      ])
    ]),
    trigger('selectOption', [
      state('ready', style({ opacity: 1 })),
      transition('void => ready', [
        animate('4s', keyframes([
          style({opacity: '0', transition: '1s ease', offset: 0 }),
          style({opacity: '0.5', transition: '1s ease', offset: 0.1 }),
          style({opacity: '1', transition: '1s ease', offset: 0.2 }),
          style({opacity: '0.5', transition: '1s ease', offset: 0.3 }),
          style({opacity: '1', transition: '1s ease', offset: 0.4 }),
          style({opacity: '0.5', transition: '1s ease', offset: 0.5 }),
          style({opacity: '1', transition: '1s ease', offset: 0.6 }),
          style({opacity: '0.5', transition: '1s ease', offset: 0.7 }),
          style({opacity: '1', transition: '1s ease', offset: 0.8 }),
          style({opacity: '0.5', transition: '1s ease', offset: 0.9 }),
          style({opacity: '1', transition: '1s ease', offset: 1 }),
        ]))
      ]),
      transition('ready => void', [
        style({ opacity: 0 }),
        animate('600ms 600ms ease-in-out')
      ])
    ]),
  ],
  host: { '[@slideInOutAnimation]': '' }
})
export class MobileComponent implements OnInit {

  anim: any
  backgroundAnimationConfig: object
  dropIntro: string = 'void'
  questionState: string = 'void'
  firstOptionState: string = 'void'
  secondOptionState: string = 'void'
  feedbackTextState: string = 'void'
  selectOptionState: string = 'void'
  imageUrl: string
  questionObject: Question
  enableMovement: boolean = false
  userQuestions: any


  constructor(
    private router: Router,
    private stepperService: StepperService,
    private questionsService: QuestionsService,
  ) {

  }

  ngOnInit() {
    this.getCurrentQuestion();
    this.userQuestions = JSON.parse(sessionStorage.getItem('questionsList'))

    setTimeout(() => {
      this.dropIntro = 'ready'
      this.questionState = 'show'
    }, 1800)

    setTimeout(() => {
      this.questionState = 'ready'
      this.firstOptionState = 'ready'
      this.secondOptionState = 'ready'
    }, 2500)

    setTimeout(() => {
      this.selectOptionState = 'ready'
      this.enableMovement = true
    }, 4500)

    this.getCurrentQuestion();

  }

  selectOptionA(selected) {
    this.secondOptionState = 'hover'
    this.clearOptionsAndSetFeedback(selected)
  }

  selectOptionB(selected) {
    this.firstOptionState = 'hover'
    this.clearOptionsAndSetFeedback(selected)
  }

  clearOptionsAndSetFeedback(selected){
    this.dropIntro = 'void'
    this.questionState = 'void'
    this.firstOptionState = 'void'
    this.secondOptionState = 'void'
    this.selectOptionState = 'void'
    this.questionsService.setQuestionsAnswered(this.questionObject.step, selected)

    setTimeout(() => {
      this.goToNextStep()
    }, 2000)
  }

  getCurrentQuestion() {
    this.questionObject = this.stepperService.getCurrentQuestion()
  }

  goToNextStep() {
    let nextQuestion = this.stepperService.getNextQuestion()
    if((nextQuestion.step - 1) % 4 === 0){
      this.router.navigate(['spliter'])
    }else if(nextQuestion.templateMobile){
      this.router.navigate([nextQuestion.templateMobile])
    }else {
      this.router.navigate([nextQuestion.template])
    }
  }


}

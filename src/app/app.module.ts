import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {
  MatDialogModule,
  MatFormFieldModule,
  MatInputModule,
  MatButtonModule,
  MatSnackBarModule,
  MatCardModule,
  MatExpansionModule,
  MatProgressSpinnerModule,
  MatSelectModule
} from '@angular/material';
// import { Player } from '@vimeo/player';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';


import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { IntroComponent } from './components/intro/intro.component';
import { ROUTES } from './app.routes'
import { RouterModule, PreloadAllModules } from '@angular/router';
import { LottieAnimationViewModule } from 'ng-lottie';
import { QuestionsService } from './services/questions.service';
import { QuestionTwoOptionsImageComponent } from './components/question-two-options-image/question-two-options-image.component';
import { QuestionTwoOptionsImageTrapezoidsComponent } from './components/question-two-options-image-trapezoids/question-two-options-image-trapezoids.component';
import { QuestionTwoOptionsImageCircleComponent } from './components/question-two-options-image-circle/question-two-options-image-circle.component';
import { QuestionTwoImagesComponent } from './components/question-two-images/question-two-images.component';
import { TieBreakerComponent } from './components/tie-breaker/tie-breaker.component';
import { MobileComponent } from './components/mobile/mobile.component';
import { ProgressBarComponent } from './components/progress-bar/progress-bar.component';
import { SpliterComponent } from './components/spliter/spliter.component';
import { PreResultComponent } from './components/pre-result/pre-result.component';
import { ResultsComponent } from './components/results/results.component';
import { StepperService } from './services/stepper.service';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    IntroComponent,
    QuestionTwoOptionsImageComponent,
    QuestionTwoOptionsImageTrapezoidsComponent,
    QuestionTwoOptionsImageCircleComponent,
    QuestionTwoImagesComponent,
    TieBreakerComponent,
    MobileComponent,
    ProgressBarComponent,
    SpliterComponent,
    PreResultComponent,
    ResultsComponent,
  ],
  imports: [
    BrowserModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatSnackBarModule,
    MatCardModule,
    MatExpansionModule,
    MatProgressSpinnerModule,
    MatSelectModule,
    HttpClientModule,
    BrowserAnimationsModule,
    LottieAnimationViewModule.forRoot(),
    RouterModule.forRoot(ROUTES, {preloadingStrategy: PreloadAllModules}),
  ],
  providers: [{provide: LocationStrategy, useClass: HashLocationStrategy},
    QuestionsService, StepperService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

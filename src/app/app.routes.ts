import { NgModule } from '@angular/core';
import { Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { IntroComponent } from './components/intro/intro.component';
import { QuestionTwoOptionsImageTrapezoidsComponent } from './components/question-two-options-image-trapezoids/question-two-options-image-trapezoids.component';
import { QuestionTwoOptionsImageCircleComponent } from './components/question-two-options-image-circle/question-two-options-image-circle.component';
import { QuestionTwoOptionsImageComponent } from './components/question-two-options-image/question-two-options-image.component';
import { QuestionTwoImagesComponent } from './components/question-two-images/question-two-images.component';
import { MobileComponent } from './components/mobile/mobile.component';
import { SpliterComponent } from './components/spliter/spliter.component';
import { PreResultComponent } from './components/pre-result/pre-result.component';
import { ResultsComponent } from './components/results/results.component';
import { TieBreakerComponent } from './components/tie-breaker/tie-breaker.component';

export const ROUTES: Routes = [
  { path: '', component: LoginComponent},
  { path: 'OTXVm5HsrnU5z8MBnabJWCQnYYNq5qM1s2VmovNzIU5D0waFoY', component: IntroComponent},
  { path: '4sUA9LCWtZNEftCXWDcg9fT4Cg5X6Ca50OjLOAs9C3kntFN3qf', component: QuestionTwoOptionsImageTrapezoidsComponent},
  { path: 'KFwbHRvlLmfRex5zHl3mx2qKHDOVrCFctLBjcqtBDjEcpQz3Hm', component: QuestionTwoOptionsImageTrapezoidsComponent},
  // { path: 'question-two-options-image-circle', component: QuestionTwoOptionsImageCircleComponent},
  // { path: 'question-two-options-image', component: QuestionTwoOptionsImageComponent},
  { path: 'd9TxqMOgBhB9L2w6uc2qKSUwjvwKSuxMwW2OZaM0PwV6iT6Ab8', component: QuestionTwoImagesComponent},
  { path: 'FaaaTbebnES4xj6VKJLss75NTeX1k42LDUZmmYDC9wjltCB8Mw', component: QuestionTwoImagesComponent},
  { path: 'xlHiM9xiHxMJv53OYZM4E8sn7hCRC6J8lbrgvuuJkfR7pkZngp', component: TieBreakerComponent},
  { path: '8LYF9DJ5EXsjkHyTQlwfjQRsvQiaKUW32qFBeGwj0pAITYoXMY', component: MobileComponent},
  { path: 'OgiQBS69QRYYRTAIjlrfJRu2LgpxNb3wdD9dI13HmyYzn0S31H', component: MobileComponent},
  { path: '0OB1cztzwtmxwVQVlD00FPdocl1jeV8bZKUxtDbzb09J7FAgLq', component: SpliterComponent},
  { path: 'pre-result', component: PreResultComponent},
  { path: 'results', component: ResultsComponent},
];

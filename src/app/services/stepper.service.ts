import { QuestionsService } from './questions.service';
import { Injectable} from '@angular/core'
import { Question } from '../models/question.model';
import { PlatformLocation } from '@angular/common'
import { Router, NavigationStart } from '@angular/router';

@Injectable()
export class StepperService {

    questionList: Array<Question>
    currentStep: string
    prevStep: any
    nextStep: any
    lastStep: any
    currentQuestion: object
    nextQuestion: any
    previousQuestion: any
    templateArray = [ 'd9TxqMOgBhB9L2w6uc2qKSUwjvwKSuxMwW2OZaM0PwV6iT6Ab8', '4sUA9LCWtZNEftCXWDcg9fT4Cg5X6Ca50OjLOAs9C3kntFN3qf','FaaaTbebnES4xj6VKJLss75NTeX1k42LDUZmmYDC9wjltCB8Mw', 'KFwbHRvlLmfRex5zHl3mx2qKHDOVrCFctLBjcqtBDjEcpQz3Hm',
                      'xlHiM9xiHxMJv53OYZM4E8sn7hCRC6J8lbrgvuuJkfR7pkZngp', '8LYF9DJ5EXsjkHyTQlwfjQRsvQiaKUW32qFBeGwj0pAITYoXMY', 'OgiQBS69QRYYRTAIjlrfJRu2LgpxNb3wdD9dI13HmyYzn0S31H', 'results']
    imageList: any

    constructor(
        private location: PlatformLocation,
        private router: Router,
        private questionsService: QuestionsService
    ) {
        let prevStepUrl,
            currentStepUrl,
            nexSteptUrl

        router.events.subscribe((val) => {
          if(this.checkTemplate()){
            if(val instanceof NavigationStart){
                let questions = JSON.parse(sessionStorage.getItem('questionsList')).questions,
                currentStep:number = parseInt(sessionStorage.getItem('currentStep'))

                if(currentStep > 1 && currentStep < questions.length) {
                    prevStepUrl = questions[currentStep - 2].template
                    currentStepUrl = questions[currentStep -1].template
                    nexSteptUrl = questions[currentStep].template
                }
            }
          }
        })

        location.onPopState(() => {
            let questionsLength = JSON.parse(sessionStorage.getItem('questionsList')).questions.length,
                currentStep:number = parseInt(sessionStorage.getItem('currentStep')),
                currentUrl = location.hash

            if(currentStep > 0 && currentStep < questionsLength) {
                if(currentUrl == `#/${prevStepUrl}`) {
                    currentStep--
                }
                if(currentStep == 1  && currentUrl == `#/${currentStepUrl}`) {
                    currentStep++
                }

                if(currentStep > 1  && currentUrl == `#/${nexSteptUrl}`) {
                    currentStep++
                }
                sessionStorage.setItem('currentStep', JSON.stringify(currentStep))
            }
        });
    }

    detectIe(){
       if(navigator.userAgent.match(/msie/i)
          || navigator.userAgent.match(/trident/i)
          || navigator.userAgent.match(/edge/i)
          || navigator.userAgent.match(/firefox/i)
          || (navigator.userAgent.match(/safari/i) && !navigator.userAgent.match(/chrome/i))) {
          return true;
       } else {
         return false;
       }
    }

    detectmob() {
       if( navigator.userAgent.match(/Android/i)
       || navigator.userAgent.match(/webOS/i)
       || navigator.userAgent.match(/iPhone/i)
       || navigator.userAgent.match(/iPad/i)
       || navigator.userAgent.match(/iPod/i)
       || navigator.userAgent.match(/BlackBerry/i)
       || navigator.userAgent.match(/Windows Phone/i)
       ){
          return true;
        }
       else {
          return false;
        }
    }

    checkTemplate(){
      return this.templateArray.includes(window.location.href.split('/').pop());
    }

    getCurrentQuestion() {
        this.questionList = JSON.parse(sessionStorage.getItem('questionsList')).questions
        this.currentStep = sessionStorage.getItem('currentStep')
        this.currentQuestion = this.questionList.filter((question) =>{
            if(question.step == parseInt(this.currentStep)){
                return question
            }
        })

        return this.currentQuestion[0]
    }

    getImages(){
    return new Promise((resolve)=> {
      if(this.imageList){
          resolve(this.imageList);
        } else{
          this.questionsService.getQuestions()
            .subscribe(res => {
              sessionStorage.setItem('questionsList', JSON.stringify(res.PT));
              var images = new Array()
              let questions = res.PT.questions;
              for (let i = 0; i < questions.length; i++) {
                  images[i] = {
                    image1: new Image(),
                    image2: new Image(),
                    step: questions[i].step
                  }
                  images[i].image1.src = questions[i].options[0].image;
                  images[i].image2.src = questions[i].options[1].image;
              }
              this.imageList = images;
              resolve(this.imageList);
            });
        }
      })
    }

    setImages(){
      this.questionsService.getQuestions()
        .subscribe(res => {
          sessionStorage.setItem('questionsList', JSON.stringify(res.PT));
          var images = new Array()
          let questions = res.PT.questions;
          for (let i = 0; i < questions.length; i++) {
              images[i] = {
                image1: new Image(),
                image2: new Image(),
                step: questions[i].step
              }
              images[i].image1.src = questions[i].options[0].image;
              images[i].image2.src = questions[i].options[1].image;
          }
          this.imageList = images;
        })
    }

    getNextQuestion() {
        this.questionList = JSON.parse(sessionStorage.getItem('questionsList')).questions
        this.nextStep = parseInt(sessionStorage.getItem('currentStep')) + 1

        this.nextQuestion = this.questionList.filter((question) =>{
            if(question.step == parseInt(this.nextStep)){
                return question
            }
        })
        if(this.nextQuestion.length > 0) {
            if(this.nextStep == (this.questionList.length)) {
                if(this.checkIfHasDraw()) {
                    sessionStorage.setItem('currentStep', this.nextStep)
                    return this.nextQuestion[0]
                } else {
                    return {template: 'pre-result', templateIE: 'pre-result', templateMobile: 'pre-result'}
                }
            } else {sessionStorage.setItem('currentStep', this.nextStep)
                return this.nextQuestion[0]
            }
        } else {
            return {template: 'pre-result', templateIE: 'pre-result', templateMobile: 'pre-result'}
        }
    }

    getPreviousQuestion() {
        this.questionList = JSON.parse(sessionStorage.getItem('questionsList')).questions
        this.lastStep = parseInt(sessionStorage.getItem('currentStep')) - 1

        this.previousQuestion = this.questionList.filter((question) =>{
            if(question.step == parseInt(this.lastStep)){
                return question
            }
        })
        return this.previousQuestion;
    }

    checkIfHasDraw() {
        return this.questionsService.getProfileResult() === 'draw'
    }


}

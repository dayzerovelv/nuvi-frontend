import { Option } from './../models/option.model';
import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs/Observable'
import { API_URL } from '../app.api'

@Injectable()
export class QuestionsService {

    answeredForm: Array<any>
    currentStep: number
    occurrences: any

    constructor(
        private http: HttpClient
    ) {
    }

    getQuestions(){
        return this.http.get<any>('./assets/questions-list-mock.json')
    }

    getArchetypes(): Observable<any> {
        return this.http.get<any>('assets/archetypes.json')
    }

    getSurvey(): Observable<any> {
        let token = sessionStorage.getItem('token'),
            language = sessionStorage.getItem('language')

         return this.http.get<any>(`${API_URL}/survey/${language}/GalpExpand?token=${token}`)
    }

    setQuestionsAnswered(step, selected) {
        this.answeredForm = JSON.parse(sessionStorage.getItem('answeredForm')) || []

        let answer = {
            step: step,
            option: selected
        }

        let answeredQuestion = this.answeredForm.filter(answer => answer.step === step);

        if (answeredQuestion[0]) {
            this.answeredForm.forEach((element, index) => {
                if (element.step === answeredQuestion[0].step) {
                    this.answeredForm[index] = answer
                }
            });
        } else {
            this.answeredForm.push(answer)
        }

        sessionStorage.setItem('answeredForm', JSON.stringify(this.answeredForm))
    }

    getProfileResult() {
        let scoreBoard = [
            {
              'name': 'Explorador',
              'value': this.checkWordFrequency('Explorador')
            },
            {
              'name': 'Herói',
              'value': this.checkWordFrequency('Herói')
            },
            {
              'name': 'Guardião',
              'value': this.checkWordFrequency('Guardião')
            },
            {
              'name': 'Elo',
              'value': this.checkWordFrequency('Elo')
            },
            {
              'name': 'Mentor',
              'value': this.checkWordFrequency('Mentor')
            }].sort(function(a, b) {return b.value - a.value})

            let hasDraw = scoreBoard[1].value == scoreBoard[0].value ? true : false;

        if (hasDraw) {
            if((window.location.href.split('/').pop() === 'pre-result') || (window.location.href.split('/').pop() === 'xlHiM9xiHxMJv53OYZM4E8sn7hCRC6J8lbrgvuuJkfR7pkZngp')){
              return scoreBoard
            }else {
              return 'draw'
            }
        } else{
          return scoreBoard;
        }

    }

    checkWordFrequency(type) {
        let answeredForm = JSON.parse(sessionStorage.getItem('answeredForm'))

        this.occurrences = answeredForm.reduce((obj, item) => {
            obj[item.option.archetype] = (obj[item.option.archetype] || 0) + 1;
            return obj;
        }, {});

        return this.occurrences[type]
    }

    saveResults(data) {
        return this.http.post<any>(`${API_URL}/userdata/save`, data)
    }

}

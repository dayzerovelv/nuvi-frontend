import { Component } from '@angular/core';
import { Howl, Howler } from 'howler';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app';

  isPlaying: boolean = true
  sound: Howl

  ngOnInit() {

      this.sound = new Howl({
      loop: true,
      src: ['./assets/audio/instrumental.mp3']
    });

    Howler.volume(0.3)
    this.sound.play()
    Howler.volume(0.3)
  }

  toggleSound(){
    if(this.isPlaying){
      this.sound.pause();
    } else{
      this.sound.play();
    }
    this.isPlaying = !this.isPlaying
  }


}

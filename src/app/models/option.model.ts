export interface Option {
    answer: string
    feedback: string
    imageUrl: string
    imageHoverUrl: string
    archetype: string
    author: string
}
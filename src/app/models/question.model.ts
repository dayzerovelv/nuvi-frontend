import { Option } from "./option.model";

export interface Question {
    question: string
    template: string
    templateUrl: string
    step: number
    options: Array<Option>
    image: string
    description: string
}

